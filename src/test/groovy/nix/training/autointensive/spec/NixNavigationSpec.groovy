package nix.training.autointensive.spec

import geb.spock.GebReportingSpec
import nix.training.autointensive.page.StartPage
import nix.training.autointensive.page.BlogPage

class NixNavigationSpec extends GebReportingSpec{

        def "Navigate to main page"(){
            when:
            to StartPage

            and:
            "User navigates to blog page"()

            then:
            at BlogPage
    }
}
